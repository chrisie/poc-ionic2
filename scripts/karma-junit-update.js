let fs = require('fs'),
    xml2js = require('xml2js');

let xmlFileToJs = (filepath) => {
    return new Promise((resolve, reject) => {
        fs.readFile(filepath, 'utf8', (err, xmlStr) => {
            if (err) {
                reject(err);
            }
            new xml2js.Parser().parseString(xmlStr, (err, result) => {
                if (err) {
                    reject(err)
                }
                resolve(result);
            });
        });
    });
};

let jsToXmlFile = (filepath, obj) => {
    return new Promise((resolve, reject) => {
        var builder = new xml2js.Builder();
        var xml = builder.buildObject(obj);
        fs.writeFile(filepath, xml, (err) => {
            if (err) {
                reject(err);
            }
            resolve();
        });
    });
};

let path = 'output/unit/output.xml';
return xmlFileToJs(path).then((xmlData) => {
    let updatedData = {'testsuites': xmlData};
    return jsToXmlFile(path, updatedData);
});
