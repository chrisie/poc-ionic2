echo 'Executing buddybuild midcordova'
source .ctpinit

npm run test:unit
status=$?

node scripts/karma-junit-update.js

#Copy unit test results. At this stage we don't have the $BUDDYBUILD_CUSTOM_TEST_RESULTS variable
#available so we use the folder name directly
if [ -z "$BUDDYBUILD_CUSTOM_TEST_RESULTS" ]; then
    echo 'BUDDYBUILD_CUSTOM_TEST_RESULTS is unset. Setting to /tmp/sandbox/workspace/buddybuild_artifacts'
    export BUDDYBUILD_CUSTOM_TEST_RESULTS=/tmp/sandbox/workspace/buddybuild_artifacts
fi

echo 'Test results folder is ' $BUDDYBUILD_CUSTOM_TEST_RESULTS
if [ ! -d "$BUDDYBUILD_CUSTOM_TEST_RESULTS" ]; then
  mkdir $BUDDYBUILD_CUSTOM_TEST_RESULTS
fi
mkdir $BUDDYBUILD_CUSTOM_TEST_RESULTS/Karma
cp $BUDDYBUILD_WORKSPACE/output/unit/output.xml $BUDDYBUILD_CUSTOM_TEST_RESULTS/Karma/output.xml

if [ $status -ne 0 ]; then
    echo 'Unit tests failed'
    exit 1
else
    echo 'Unit tests succeeded'
fi
