#Only run functional tests on iOS
if [ -z "$BUDDYBUILD_SCHEME" ]; then
  echo 'Not an iOS build - skipping functional tests'
else
  echo 'Executing functional tests'

  #npm install -g authorize-ios
  #sudo authorize-ios

  brew install graphicsmagick
  npm run test:func
  status=$?

  #Convert Cucumber JSON output to JUnit XML
  echo 'Test results folder is ' $BUDDYBUILD_CUSTOM_TEST_RESULTS
  if [ ! -d "$BUDDYBUILD_CUSTOM_TEST_RESULTS" ]; then
    mkdir $BUDDYBUILD_CUSTOM_TEST_RESULTS
  fi
  mkdir $BUDDYBUILD_CUSTOM_TEST_RESULTS/Appium

  cat $BUDDYBUILD_WORKSPACE/output/functional/test.json | $BUDDYBUILD_WORKSPACE/node_modules/.bin/cucumber-junit-enhance > $BUDDYBUILD_CUSTOM_TEST_RESULTS/Appium/xmloutput.xml

  if [ $status -ne 0 ]; then
    echo 'Functional tests failed'
    exit 1
  else
    echo 'Functional tests succeeded'
  fi
fi
